// used for routing
import {ModuleWithProviders} from '@angular/core';
// router related modules
import {Routes,RouterModule} from '@angular/router';
//bring in components used in RouterModule
import {UserComponent} from './components/user.component';
import {AboutComponent} from './components/about.component';

// define a varible const
const appRoutes: Routes = [
{ path:'',
  component:UserComponent
},
{ path:'about',
  component: AboutComponent,
}

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
