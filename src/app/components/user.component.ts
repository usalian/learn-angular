import { Component } from '@angular/core';
import {Injectable } from '@angular/core';
import {PostsService} from '../services/posts.service';

@Component({
  moduleId: module.id,
  selector: 'user',
  templateUrl:  'user.component.html',
  providers : [PostsService],
})
export class UserComponent  {
  name: string;
  email: string;
  address : address;
 hobbies: string[];
showHobbies:boolean;
 posts: Post[];

    constructor(private postsService:PostsService) {
      this.name = 'Angular';
     this.email = 'usalian@gmail.com'
      this.address = {
        street : '4227 Tessie ct',
        city : 'Sugar Land',
        state: 'TX'
    }

    this.hobbies = ['Music','Movies'];
    this.showHobbies =  true;
    //subscribe
    this.postsService.getPosts().subscribe(posts => { this.posts= posts;});
 }
 toggleHobbies(){
   this.showHobbies = !this.showHobbies;
 }
 addHobby(newHobby) {
   console.log(newHobby);
   this.hobbies.push(newHobby);
 }
 deleteHobby(index){
   this.hobbies.splice(index,1);
 }
}
interface address {
  street: string;
  city : string;
  state : string;
}
interface Post {
  id : number;
  title : string;
  body : string;

}
